 <div class="slider-section">
            <div class="tp-banner-container">
                <div class="tp-banner">
                    <ul>
					<?php foreach($slider->result() as $row){?>
                        <li data-transition="fade" data-slotamount="1" data-masterspeed="500" data-thumb="assets/uploads/slider/<?php echo $row->file;?>"  data-saveperformance="off"  data-title="Garden">
                            <img src="assets/uploads/slider/<?php echo $row->file;?>"  alt="fullslide1"  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                            <div class="tp-caption slider_layer_01 text-center lft tp-resizeme"
                                data-x="center"
                                data-y="295" 
                                data-speed="1000"
                                data-start="1200"
                                data-easing="Power3.easeInOut"
                                data-splitin="none"
                                data-splitout="none"
                                data-elementdelay="0.1"
                                data-endelementdelay="0.1"
                                data-endspeed="1000"
                                style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;"><?php echo $row->nev;?>
                            </div>
                        </li>
					<?php }?>
                    </ul>
                </div>
            </div>
        </div>
