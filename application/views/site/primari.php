<body>  

    <div id="loader">
        <div class="loader-container">
            <img src="images/load.gif" alt="" class="loader-site spinner">
        </div>
    </div>

    <div id="wrapper">
        <div class="topbar clearfix">
            <div class="container">
                <div class="row-fluid">
                    <div class="col-md-6 text-left">
                        <div class="social">
                            <a href="#" data-tooltip="tooltip" data-placement="bottom" title="Facebook"><i class="fa fa-facebook"></i></a>              
                            <a href="#" data-tooltip="tooltip" data-placement="bottom" title="Twitter"><i class="fa fa-twitter"></i></a>
                            <a href="#" data-tooltip="tooltip" data-placement="bottom" title="Google Plus"><i class="fa fa-google-plus"></i></a>
                            <a href="#" data-tooltip="tooltip" data-placement="bottom" title="Linkedin"><i class="fa fa-linkedin"></i></a>
                            <a href="#" data-tooltip="tooltip" data-placement="bottom" title="Youtube"><i class="fa fa-youtube"></i></a>
                            <a href="#" data-tooltip="tooltip" data-placement="bottom" title="Pinterest"><i class="fa fa-pinterest"></i></a>
                        </div><!-- end social -->
                    </div><!-- end left -->
                    <div class="col-md-6 text-right">
                        <p>
                            <strong><i class="fa fa-phone"></i></strong> <?php echo $beallitasok->mobil;?> &nbsp;&nbsp;
                            <strong><i class="fa fa-envelope"></i></strong> <a href="mailto:<?php echo $beallitasok->nyilvanosemail;?>"><?php echo $beallitasok->nyilvanosemail;?></a>
                        </p>
                    </div><!-- end left -->
                </div><!-- end row -->
            </div><!-- end container -->
        </div><!-- end topbar -->

        <header class="header">
            <div class="container">
                <div class="row mb-5">
                    <div class="col-md-12">
                        <nav class="navbar yamm navbar-default">
                            <div class="container-full">
                                <div class="navbar-table">
                                    <div class="navbar-cell">
                                        <div class="navbar-header">
                                            <a class="navbar-brand" href="<?php echo base_url();?>"><img src="assets/uploads/files/<?php echo $beallitasok->logo;?>" alt="Ova"></a>
                                            <div>
                                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
                                                    <span class="sr-only">Menü</span>
                                                    <span class="fa fa-bars"></span>
                                                </button>
                                            </div>
                                        </div><!-- end navbar-header -->
                                    </div><!-- end navbar-cell -->

                                    <div class="navbar-cell stretch">
                                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                                            <div class="navbar-cell">
                                                <ul class="nav navbar-nav navbar-right">
													<li><a href="rolunk">Rólunk</a></li>
                                                    <li class="dropdown has-submenu">
                                                        <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Beltéri ajtók<span class="fa fa-angle-down"></span></a>
                                                        <ul class="dropdown-menu start-left" role="menu">
                                                            <li><a href="ajto_felujitasa">Ajtó felújítása</a></li>
                                                            <li><a href="tokfelujitas">Tokfelújítás</a></li>
                                                            <li><a href="otletado">Ötletadó</a></li>
                                                            <li><a href="garancia">Garancia</a></li>
                                                        </ul>
                                                    </li>
                                                    <li class="dropdown has-submenu">
                                                        <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Beltéri ajtók<span class="fa fa-angle-down"></span></a>
                                                        <ul class="dropdown-menu start-left" role="menu">
                                                            <li><a href="bejarati_ajtok_felujitasa">Bejárati ajtók felújítása</a></li>
															<li><a href="minoseg_es_biztonsag">Minőség és biztonság</a></li>
                                                        </ul>
                                                    </li>
                                                    <li><a href="elotetok">Előtetők</a></li>
													<li><a href="galeria">Galéria</a></li>
                                                    <li><a href="kapcsolat">Kapcsolat</a></li>
                                                </ul>
                                            </div><!-- end navbar-cell -->
                                        </div><!-- /.navbar-collapse -->        
                                    </div><!-- end navbar cell -->
                                </div><!-- end navbar-table -->
                            </div><!-- end container fluid -->
                        </nav><!-- end navbar -->
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </header>
