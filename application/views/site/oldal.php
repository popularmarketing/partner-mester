<?php include('header.php');?>
<?php include('primari.php');?>
        <div class="page-title grey">
            <div class="container">
                <div class="title-area text-center">
                    <h2><?php echo $oldal->nev?></h2>
                    <div class="bread">
                        <ol class="breadcrumb">
                            <li><a href="#">Főoldal</a></li>
                            <li class="active"><?php echo $oldal->nev?></li>
                        </ol>
                    </div><!-- end bread -->
                </div><!-- /.pull-right -->
            </div>
        </div><!-- end page-title -->

        <section class="section white">
            <div class="container">
                <div class="row">
                    <div id="content" class="col-md-8">
                       <?php print_r($oldal->tartalom)?>
                    </div>

                    <div id="sidebar" class="col-md-4 col-sm-12 col-xs-12">

                        <div class="widget">
                            <div class="widget-title">
                                <h4>Új képek</h4>
                                <hr>
                            </div><!-- end title -->

                            <div class="recent-post-widget">
                                <ul class="recent-posts">
									<?php foreach($galeria3->result() as $row){?>
										<li>
											<p><a href="galeria" title=""><img src="assets/uploads/files/<?php echo $row->file?>" alt="" class="alignleft"><?php echo $row->nev?></a></p>
										</li>
									<?php }?>
                                </ul><!-- end latest-tweet -->
                            </div><!-- end twitter-widget -->
                        </div><!-- end widget -->

                        <div class="widget">
                            <div class="widget-title">
                                <h4>Nyitvatartás</h4>
                                <hr>
                            </div><!-- end title -->

                            <div class="cats-widget">
                                <?php print_r($beallitasok->nyitvatartas);?>
                            </div><!-- end twitter-widget -->
                        </div><!-- end widget -->


                        <div class="widget">
                            <div class="widget-title">
                                <h4>Elérési lehetőségek</h4>
                                <hr>
                            </div><!-- end title -->

                            <div class="twitter-widget">
                                <ul>
                                    <li>
                                        Telefon: <?php echo $beallitasok->mobil?>
                                    </li>
                                    <li>
                                        Email: <?php echo $beallitasok->nyilvanosemail?>
                                    </li>
                                </ul><!-- end latest-tweet -->
                            </div><!-- end twitter-widget -->
                        </div><!-- end widget -->
                    </div><!-- end content -->  
                </div>
            </div><!-- end container -->
        </section><!-- end section -->
<?php include('footer.php');?>