<?php include('header.php');?>
<?php include('primari.php');?>
        <div class="page-title grey">
            <div class="container">
                <div class="title-area text-center">
                    <h2>Galéria</h2>
                    <div class="bread">
                        <ol class="breadcrumb">
                            <li><a href="#">Főoldal</a></li>
                            <li class="active">Galéria</li>
                        </ol>
                    </div><!-- end bread -->
                </div><!-- /.pull-right -->
            </div>
        </div><!-- end page-title -->

        <section class="section white">
            <div class="container">
                <div class="row module-wrapper grid-layout text-center">
				
				<?php foreach($galeria->result() as $row){?>
                    <div class="col-md-4 col-sm-4 shop-item">
                        <div class="entry">
							<a class="st" rel="bookmark" data-rel="prettyPhoto" href="assets/uploads/files/<?php echo $row->file?>">
								<img class="img-responsive" src="assets/uploads/files/<?php echo $row->file?>" alt="" style="margin:auto;">
								<div class="magnifier">
								</div><!-- end magnifier -->
							</a>
                        </div><!-- end entry -->
                        <h4 align="center"><?php echo $row->nev?></h4>
                    </div><!-- end gallery-item -->
				<?php }?>
					
                </div><!-- end row -->
            </div><!-- end container -->
        </section><!-- end section -->
<?php include('footer.php');?>