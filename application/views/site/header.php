<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="keywords" content="">

    <title><?php echo $oldal->nev;?> | <?php echo $beallitasok->oldalnev;?></title>

    <link rel="shortcut icon" href="<?php echo base_url();?>/assets/uploads/files/<?php echo $beallitasok->favicon?>" type="image/x-icon">
    <link rel="apple-touch-icon" href="<?php echo base_url();?>/images/apple-touch-icon.png" />
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url();?>/images/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url();?>/images/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url();?>/images/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url();?>/images/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url();?>/images/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url();?>/images/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url();?>/images/apple-touch-icon-152x152.png">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/rs-plugin/css/settings.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/css/animate.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/css/carousel.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/css/bootstrap-select.css">
	<link rel="stylesheet" type="text/css" href="css/prettyPhoto.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/css/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/css/style.css">

    <!-- COLORS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/css/custom.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>