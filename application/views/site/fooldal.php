<?php include('header.php');?>
<?php include('primari.php');?>
<?php include('slider.php');?>
        <section class="section grey">
            <div class="container">
                <div class="general-title text-center">
                    <h4>Mit ajánlunk</h4>
                    <hr>
                </div><!-- end general title -->

                <div class="row module-wrapper text-center">
					<div class="col-sm-7" align="left"><h3>Korszerű, hosszú távú és tartós megoldás!</h3><p>Jól tudjuk, hogy a meglévő ajtók cseréje milyen zajjal, törmelékkel, előre pontosan nem meghatározható kellemetlenségekkel járhat, mint például a vakolat-, tapéta-, csempe- vagy padlóburkolat sérülései. Ezzel szemben az ajtófelújításra kidolgozott PORTAS-megoldás tökéletes technológiát, minőséget és szolgáltatást kínál minden megrendelőjének. <b>Akár 1 nap alatt!</b></p></div>
					<div class="col-sm-2"></div>
					<div class="col-sm-2"><img src="upload/nutandbolt.png" alt="" class="img-responsive wow fadeInUp"></div>
					<div class="col-sm-1"></div>
                </div><!-- end module-wrapper -->

            </div><!-- end container -->
        </section><!-- end section -->

        <section class="section darkbg fullscreen paralbackground parallax" style="background-image:url('upload/parallax_01.jpg');" data-img-width="1688" data-img-height="1125" data-diff="100">
            <div class="overlay customoverlay"></div>
            <div class="container">
                <div class="row text-center">
                    <div class="col-md-5">
                         <div class="text-center image-center">
                            <img src="upload/repairman.png" alt="" class="img-responsive wow fadeInUp">
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="videobg text-right">
                            <h1>Rólunk</h1>
                            <p><?php echo $oldal->tartalom?></p> 
                        </div>
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </section><!-- end section -->

        

        <section class="section grey">
            <div class="container">
                <div class="general-title text-center">
                    <h4>Pár kép a galériából</h4>
                    <p class="lead">Itt megtalálható pár kép, a galériából</p>
                    <hr>
                </div><!-- end general title -->

                <div class="row module-wrapper blog-widget">
				<?php foreach($galeria->result() as $row){ ?>
                    <div class="col-md-4 col-sm-6">
                        <div class="blog-wrapper">
                            <div class="blog-title">
                                <a class="category_title" href="galeria" title=""><?php echo $row->nev;?></a>
                                <div class="blog-image">
                                    <a href="galeria" title=""><img src="assets/uploads/files/<?php echo $row->file;?>" alt="" class="img-responsive"></a>
                                </div><!-- end image -->
                            </div><!-- end desc -->
                        </div><!-- end blog-wrapper -->
                    </div><!-- end col -->
				<?php }?>
                </div><!-- end row -->
            </div><!-- end container -->
        </section><!-- end section -->
<?php include('footer.php');?>