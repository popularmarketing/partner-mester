<?php include('header.php');?>
<?php include('primari.php');?>
        <div class="page-title grey">
            <div class="container">
                <div class="title-area text-center">
                    <h2>Nem találva</h2>
                    <div class="bread">
                        <ol class="breadcrumb">
                            <li><a href="#">Főoldal</a></li>
                            <li class="active">404 Hiba</li>
                        </ol>
                    </div><!-- end bread -->
                </div><!-- /.pull-right -->
            </div>
        </div><!-- end page-title -->

        <section class="section white">
            <div class="container">
                <div class="row">
                    <div id="content" class="col-md-12">
                    <div class="notfound text-center">
                        <h1>404 - Az oldalt nem találtuk</h1>

                        <blockquote>Mindenhol kerestük, de sajnos nem találjuk azt az oldalt amit keres. Menjen vissza a főoldalra, és próbálkozzon újra!</blockquote>

                        <a href="<?php echo base_url();?>" class="btn btn-primary">Vissza a főoldalra</a>
                    </div>
                    </div>
                </div>
            </div><!-- end container -->
        </section><!-- end section -->
<?php include('footer.php');?>