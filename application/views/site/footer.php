        <section class="callout-section grey">
            <div class="container">
                <div class="row callout text-left">
                    <div class="col-md-7">
                        <h3>Felkeltettük a figyelmét?</h3>
                        <p class="lead">Cégünk törekszik a minimális hangerőre és a maximum hatékonyságra. Ezenfelül mi végezzük legolcsóbban az ajtófelújítási munkát a környéken.</p>
                        <a href="kapcsolat" class="btn btn-default">Lépjen velünk kapcsolatba!</a>
                    </div>

                    <div class="col-md-5 hidden-xs hidden-sm">
                         <div class="text-center image-center image-center2">
                            <img src="upload/man.png" alt="" class="img-responsive wow fadeInUp">
                        </div>
                    </div>
                </div><!-- end callout -->
            </div><!-- end container -->
        </section><!-- end section -->

        <footer class="footer">
            <div class="container">
                <div class="row module-wrapper">
                    <div class="col-md-6 col-sm-12">
						<div class="widget">
							<div class="widget-title">
								<h4>Korszerű, hosszú távú és tartós megoldás!</h4>
							</div>
							<p>Jól tudjuk, hogy a meglévő ajtók cseréje milyen zajjal, törmelékkel, előre pontosan
nem meghatározható kellemetlenségekkel járhat, mint például a vakolat-, tapéta-,
csempe- vagy padlóburkolat sérülései.

Ezzel szemben az ajtófelújításra kidolgozott PORTAS-megoldás tökéletes
technológiát, minőséget és szolgáltatást kínál minden megrendelőjének. <b>Akár 1 nap alatt!</b></p>
						</div>
                    </div><!-- end col -->

                    <div class="col-md-3 col-sm-6">
                        <div class="widget">
                            <div class="widget-title">
                                <h4>Elérhetőségeink</h4>
                            </div>
                            <ul class="site-links">
                                <li><i class="fa fa-link"></i> <?php echo base_url();?></li>
                                <li><i class="fa fa-envelope"></i> <?php echo $beallitasok->nyilvanosemail;?></li>
                                <li><i class="fa fa-phone"></i> <?php echo $beallitasok->mobil;?></li>
                                <li><i class="fa fa-home"></i> <?php echo $beallitasok->uzletcim;?></li>
                            </ul>
                        </div><!-- end widget -->
                    </div><!-- end col -->

                    <div class="col-md-3 col-sm-6">
                        <div class="widget">
                            <div class="widget-title">
                                <h4>Hasznos linkek</h4>
                            </div>
                            <ul class="site-links">
                                <li><a href="<?php echo base_url();?>">Főoldal</a></li>
                                <li><a href="ajtok_felujitasa">Ajtók felújítása</a></li>
                                <li><a href="arak">Árak</a></li>
                                <li><a href="bejarati_ajtok_felujitasa">Bejárati ajtók felújítása</a></li>
                                <li><a href="galeria">Galéria</a></li>
                                <li><a href="kapcsolat">Kapcsolat</a></li>
                            </ul>
                        </div><!-- end widget -->
                    </div><!-- end col -->
                </div><!-- end row -->
            </div>
        </footer>

        <section class="copyright">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-left">
                        <p align="center">© 2016 Minden jog fenntartva - <a href="http://popularmarketing.hu/" target="_blank">Weboldal készítés <img src="img/logo.png" alt="Popular Marketing" height="50px"></a></p>
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </section><!-- end section -->
    </div><!-- end wrapper -->

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/retina.js"></script>
    <script src="js/parallax.js"></script>
    <script src="js/wow.js"></script>
    <script src="js/carousel.js"></script>
    <script src="js/custom.js"></script>
	<!-- PORTFOLIO -->
    <script src="js/isotope.js"></script>
    <script src="js/imagesloaded.pkgd.js"></script>
    <script src="js/masonry.js"></script>  
    <script src="js/jquery.prettyPhoto.js"></script>
    <script type="text/javascript">
        (function($) {
        "use strict";
        jQuery('a[data-gal]').each(function() {
        jQuery(this).attr('rel', jQuery(this).data('gal')); });     
        jQuery("a[data-rel^='prettyPhoto']").prettyPhoto({animationSpeed:'slow',theme:'light_square',slideshow:true,overlay_gallery: true,social_tools:false,deeplinking:false});
        })(jQuery);
    </script>
    <!-- SLIDER REV -->
    <script src="rs-plugin/js/jquery.themepunch.tools.min.js"></script>
    <script src="rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
    <script>
    jQuery(document).ready(function() {
        jQuery('.tp-banner').show().revolution(
            {
            dottedOverlay:"none",
            delay:16000,
            startwidth:1170,
            startheight:600,
            hideThumbs:200,     
            thumbWidth:100,
            thumbHeight:50,
            thumbAmount:5,  
            navigationType:"none",
            navigationArrows:"solo",
            navigationStyle:"preview2",  
            touchenabled:"on",
            onHoverStop:"on",
            swipe_velocity: 0.7,
            swipe_min_touches: 1,
            swipe_max_touches: 1,
            drag_block_vertical: false,          
            parallax:"mouse",
            parallaxBgFreeze:"on",
            parallaxLevels:[10,7,4,3,2,5,4,3,2,1],
            parallaxDisableOnMobile:"off",           
            keyboardNavigation:"off",   
            navigationHAlign:"center",
            navigationVAlign:"bottom",
            navigationHOffset:0,
            navigationVOffset:20,
            soloArrowLeftHalign:"left",
            soloArrowLeftValign:"center",
            soloArrowLeftHOffset:20,
            soloArrowLeftVOffset:0,
            soloArrowRightHalign:"right",
            soloArrowRightValign:"center",
            soloArrowRightHOffset:20,
            soloArrowRightVOffset:0,  
            shadow:0,
            fullWidth:"on",
            fullScreen:"off",
            spinner:"spinner4",  
            stopLoop:"off",
            stopAfterLoops:-1,
            stopAtSlide:-1,
            shuffle:"off",  
            autoHeight:"off",           
            forceFullWidth:"off",                         
            hideThumbsOnMobile:"off",
            hideNavDelayOnMobile:1500,            
            hideBulletsOnMobile:"off",
            hideArrowsOnMobile:"off",
            hideThumbsUnderResolution:0,
            hideSliderAtLimit:0,
            hideCaptionAtLimit:0,
            hideAllCaptionAtLilmit:0,
            startWithSlide:0,
            fullScreenOffsetContainer: ""  
            }); 
        });
    </script>

    <script src="js/jquery-ui.js"></script>
    <script src="js/jquery-ui-timepicker-addon.js"></script>
    <script src="js/bootstrap-select.js"></script>
    <script type="text/javascript">
        (function($) {
        "use strict";
            $('.selectpicker').selectpicker();
            $( "#datepicker" ).datepicker();
        })(jQuery);
    </script>
</body>
</html>
<!-- Localized -->