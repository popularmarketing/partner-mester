<?php include('header.php');?>
<?php include('primari.php');?>
        <div class="page-title grey">
            <div class="container">
                <div class="title-area text-center">
                    <h2>Kapcsolat</h2>
                    <div class="bread">
                        <ol class="breadcrumb">
                            <li><a href="#">Főoldal</a></li>
                            <li class="active">Kapcsolat</li>
                        </ol>
                    </div><!-- end bread -->
                </div><!-- /.pull-right -->
            </div>
        </div><!-- end page-title -->

        <section class="section white">
            <div class="container">
                <div class="general-title text-center">
                    <h4>Elérhetőségek</h4>
                    <p class="lead">Írjon nekünk, ezen a kérdőíven keresztül!</p>
                    <hr>
                </div><!-- end general title -->

                <div class="row">
                    <div class="col-md-8">
                        <div class="appoform-wrapper noborder">

							<div class="contact_form">
								<div id="message"></div>
								<form id="contactform" class="row" action="oldal/sendmail" name="contactform" method="post">
									<fieldset class="row-fluid appoform">
										<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
											
											<input id="senderName" name="senderName" type="text" placeholder="Név" class="form-control">
										</div>
										<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
											<input type="text" name="subject" id="targy" class="form-control" placeholder="Tárgy"> 
										</div>
										<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
											<input type="text" name="email" id="email" class="form-control" placeholder="Email"> 
										</div>
										<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
											<input type="text" name="phone" id="phone" class="form-control" placeholder="Telefonszám"> 
										</div>
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
											<textarea class="form-control" name="message" id="comments" rows="6" placeholder="Üzenet"></textarea>
										</div>
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
											<button type="submit" value="Üzenet Küldése" id="submit" class="btn btn-primary btn-block btn-lg"> Üzenet Küldése</button>
										</div>
									</fieldset>
								</form> 
							</div>

                        </div><!-- end form-container -->
                    </div><!-- end col -->
                    <div class="col-md-4">
                        <div class="workinghours">
							<?php print_r($beallitasok->nyitvatartas);?>
							<br><br>
                            <ul>
                                <li>Telefon <span><?php echo $beallitasok->mobil?></span></li>
                                <li>E-Mail <span><?php echo $beallitasok->nyilvanosemail?></span></li>
                                <li>Weboldal <span><?php echo base_url();?></span></li>
                            </ul>
                        </div>
                    </div>
                </div><!-- end row -->
            </div><!-- end container -->
        </section><!-- end section -->
<?php include('footer.php');?>