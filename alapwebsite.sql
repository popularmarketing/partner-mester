-- phpMyAdmin SQL Dump
-- version 4.4.7
-- http://www.phpmyadmin.net
--
-- Gép: localhost:3306
-- Létrehozás ideje: 2016. Ápr 10. 01:04
-- Kiszolgáló verziója: 5.6.26
-- PHP verzió: 5.5.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Adatbázis: `alapwebsite`
--

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `beallitasok`
--

CREATE TABLE IF NOT EXISTS `beallitasok` (
  `id` int(11) NOT NULL,
  `oldalnev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `logo` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `favicon` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `mobil` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `vezetekes` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `fax` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `adoszam` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `telephelycim` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `uzletcim` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `nyitvatartas` text COLLATE utf8_hungarian_ci NOT NULL,
  `adminemail` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `nyilvanosemail` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `nyitva` int(1) NOT NULL,
  `nyelv` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `fooldal_title` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `fooldal_keywords` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `fooldal_description` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `google_analytics` varchar(256) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `beallitasok`
--

INSERT INTO `beallitasok` (`id`, `oldalnev`, `logo`, `favicon`, `mobil`, `vezetekes`, `fax`, `adoszam`, `telephelycim`, `uzletcim`, `nyitvatartas`, `adminemail`, `nyilvanosemail`, `nyitva`, `nyelv`, `fooldal_title`, `fooldal_keywords`, `fooldal_description`, `google_analytics`) VALUES
(1, 'Partner Mester KFT', '9a4a7-logo.png', '', '+36 (70) 419 9000', '', '', '', '', '2030 Üzlet címe', '<ul>\r\n	<li>\r\n		H&eacute;t k&ouml;zben <span>9:00 - 19:00</span></li>\r\n	<li>\r\n		H&eacute;tv&eacute;g&eacute;n <span>9:00 - 19:00</span></li>\r\n</ul>\r\n', '', 'info@partnermester.hu', 1, 'hu', '', '', '', ''),
(2, '', '', '', '', '', '', '', '', '', '', '', '', 0, 'de', '', '', '', ''),
(3, '', '', '', '', '', '', '', '', '', '', '', '', 0, 'en', '', '', '', ''),
(4, '', '', '', '', '', '', '', '', '', '', '', '', 0, 'cz', '', '', '', '');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `companytype`
--

CREATE TABLE IF NOT EXISTS `companytype` (
  `id` int(11) NOT NULL,
  `name` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `link1` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `link2` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `link3` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `link4` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `link5` varchar(256) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `companytype`
--

INSERT INTO `companytype` (`id`, `name`, `link1`, `link2`, `link3`, `link4`, `link5`) VALUES
(1, 'Étterem', '', '', '', '', ''),
(2, 'Fodrászat', '', '', '', '', ''),
(3, 'Cukrászda/Fagyizó', '', '', '', '', ''),
(4, 'Fitnesz / Gym / Edzőterem', '', '', '', '', ''),
(5, 'Tetóválószalon', '', '', '', '', ''),
(6, 'Plasztikai Sebészet', '', '', '', '', ''),
(7, 'Esküvői szalon', '', '', '', '', ''),
(8, 'Sexshop', '', '', '', '', ''),
(9, 'Ipari cikk', '', '', '', '', ''),
(10, 'Kozmetika', '', '', '', '', ''),
(11, 'Ruhazati boltok', '', '', '', '', ''),
(12, 'Utazasi iroda', '', '', '', '', ''),
(13, 'Konyveloi iroda', '', '', '', '', ''),
(14, 'Nyelviskola', '', '', '', '', ''),
(15, 'Fogászat', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `dokumentumok`
--

CREATE TABLE IF NOT EXISTS `dokumentumok` (
  `dokumentumokid` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `url` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `galeria`
--

CREATE TABLE IF NOT EXISTS `galeria` (
  `id` int(11) NOT NULL,
  `file` varchar(256) NOT NULL,
  `nev` varchar(256) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `galeria`
--

INSERT INTO `galeria` (`id`, `file`, `nev`) VALUES
(1, '1dfed-belteri_hagyomanyos_ajto_felujitasa.jpg', 'Fehér fa keretű ajtó'),
(2, 'c17ef-hagyomanyos_osztott_uveges_ajto_felujitasa.jpg', 'Barna keretű beltéri ajtó'),
(3, '6d3e5-hagyomanyos_osztott_uveges_belteri_ajto_felujitasa.jpg', 'Hagyományos üveges beltéri ajtó'),
(4, '20d13-lakotelepi_bejarati_ajto.jpg', 'Lakótelepi bejárati ajtó');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `gyarto`
--

CREATE TABLE IF NOT EXISTS `gyarto` (
  `id` int(11) NOT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `sorrend` int(10) NOT NULL,
  `statusz` int(1) NOT NULL,
  `fokep` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `title` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `keywords` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `description` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `header_custom_code` text COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(5) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `gyik`
--

CREATE TABLE IF NOT EXISTS `gyik` (
  `cim` varchar(1000) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `tartalom` text CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `sorrend` int(255) NOT NULL,
  `tag` varchar(255) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `hirek`
--

CREATE TABLE IF NOT EXISTS `hirek` (
  `id` int(11) NOT NULL,
  `url` varchar(256) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `nev` varchar(256) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `kategoria` int(11) NOT NULL,
  `lead` text CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `tartalom` text CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `datum` date NOT NULL,
  `statusz` int(11) NOT NULL,
  `kiemelt` int(11) NOT NULL,
  `fokep` varchar(256) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `tag` varchar(256) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `videoid` varchar(256) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `user` int(11) NOT NULL,
  `title` varchar(256) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `keywords` varchar(256) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `description` varchar(500) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `header_custom_code` varchar(500) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(5) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `hirek_kategoria`
--

CREATE TABLE IF NOT EXISTS `hirek_kategoria` (
  `id` int(11) NOT NULL,
  `nev` varchar(256) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(256) NOT NULL,
  `nyelv` varchar(5) NOT NULL,
  `fokep` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `kategoria`
--

CREATE TABLE IF NOT EXISTS `kategoria` (
  `kategoriaid` int(11) NOT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `tipus` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `icon` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `markericon` varchar(128) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `kategoriak`
--

CREATE TABLE IF NOT EXISTS `kategoriak` (
  `id` int(11) NOT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `szulo` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `sorrend` int(10) NOT NULL,
  `statusz` int(1) NOT NULL,
  `fokep` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `title` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `keywords` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `description` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `header_custom_code` text COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(5) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `kerdezzfelelek`
--

CREATE TABLE IF NOT EXISTS `kerdezzfelelek` (
  `id` int(11) NOT NULL,
  `nev` varchar(256) CHARACTER SET utf32 COLLATE utf32_hungarian_ci NOT NULL,
  `email` varchar(256) NOT NULL,
  `kerdes` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_hungarian_ci NOT NULL,
  `datum` datetime NOT NULL,
  `kitol` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `marka`
--

CREATE TABLE IF NOT EXISTS `marka` (
  `id` int(11) NOT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `sorrend` int(10) NOT NULL,
  `statusz` int(1) NOT NULL,
  `fokep` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `title` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `keywords` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `description` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `header_custom_code` text COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(5) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `megjelenes`
--

CREATE TABLE IF NOT EXISTS `megjelenes` (
  `megjelenesid` int(11) NOT NULL,
  `ugyfelszam` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `cegnev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `iranyitoszam` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `varos` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `cim` varchar(400) COLLATE utf8_hungarian_ci NOT NULL,
  `bevezeto` text COLLATE utf8_hungarian_ci NOT NULL,
  `kategoria` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `szoveg` text COLLATE utf8_hungarian_ci NOT NULL,
  `szoveg2` text COLLATE utf8_hungarian_ci NOT NULL,
  `youtubeid` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `fokep` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `kep1` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `kep2` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `kep3` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `kep4` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `kep5` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `telefonszam` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `email` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `mobil` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `webcim` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `nyitvatartas` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `statusz` int(1) NOT NULL,
  `letrehozva` datetime NOT NULL,
  `lejarat` datetime NOT NULL,
  `gpsx` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `gpsy` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(128) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `nyelvi_forditasok`
--

CREATE TABLE IF NOT EXISTS `nyelvi_forditasok` (
  `id` int(11) NOT NULL,
  `azonosito` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `ertek` text COLLATE utf8_hungarian_ci NOT NULL,
  `leiras` text COLLATE utf8_hungarian_ci NOT NULL,
  `file` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(5) COLLATE utf8_hungarian_ci NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `oldalak`
--

CREATE TABLE IF NOT EXISTS `oldalak` (
  `id` int(11) NOT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `tartalom` text COLLATE utf8_hungarian_ci NOT NULL,
  `szulo` int(11) NOT NULL,
  `cim` varchar(560) COLLATE utf8_hungarian_ci NOT NULL,
  `sorrend` int(10) NOT NULL,
  `statusz` int(1) NOT NULL,
  `fokep` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `title` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `keywords` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `description` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `header_custom_code` text COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(5) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `oldalak`
--

INSERT INTO `oldalak` (`id`, `nev`, `url`, `tartalom`, `szulo`, `cim`, `sorrend`, `statusz`, `fokep`, `title`, `keywords`, `description`, `header_custom_code`, `nyelv`) VALUES
(52, 'Főoldal', 'fooldal', '<p>\r\n	Arra t&ouml;reksz&uuml;nk, hogy megtartsuk az &Ouml;n ajtajait, illetve annak eg&eacute;szs&eacute;ges,<br />\r\n	j&oacute; alapanyagb&oacute;l k&eacute;sz&uuml;lt szil&aacute;rd magj&aacute;t &eacute;s az &Ouml;n ig&eacute;nyei alapj&aacute;n egy<br />\r\n	k&ouml;nnyen &aacute;polhat&oacute;, &uacute;j fel&uuml;lettel l&aacute;ssuk el.</p>\r\n<p>\r\n	Mindezt előre meghat&aacute;rozott, garant&aacute;lt, fix &aacute;ron &ndash; előre egyeztetett,<br />\r\n	&Ouml;nnek megfelelő időpontban &ndash; t&ouml;k&eacute;letes minős&eacute;gben, garanci&aacute;val v&eacute;gezz&uuml;k.</p>\r\n', 0, '', 0, 1, '', '', '', '', '', ''),
(53, '404', 'page_missing', '', 0, '', 0, 1, '', '', '', '', '', ''),
(54, 'Galéria', 'galeria', '', 0, '', 0, 1, '', '', '', '', '', ''),
(55, 'Kapcsolat', 'kapcsolat', '', 0, '', 0, 1, '', '', '', '', '', ''),
(56, 'Rólunk', 'rolunk', '<h2>\r\n	H&Iacute;REK, AKCI&Oacute;K</h2>\r\n<table border="0">\r\n	<tbody>\r\n		<tr>\r\n			<td style="padding-right:20px" valign="top">\r\n				<h4>\r\n					<strong style="color: #7d0d0b;">ESZTERGOMI BEMUTAT&Oacute;,</strong><strong style="color: #7d0d0b;"> 2015-05-14</strong></h4>\r\n				<p>\r\n					Szeretettel megh&iacute;vjuk &Ouml;n&ouml;ket a <strong>2015.</strong> <strong>m&aacute;jus 29-30-&aacute;n</strong> <em>9:00-18:00 &oacute;r&aacute;ig</em> <strong>Esztergom </strong>v&aacute;ros főter&eacute;n megrendezendő <strong>NYITOTT KAPUK NAPJ&Aacute;RA</strong>!</p>\r\n				<p>\r\n					Bemutat&oacute;nkon megismerkedhetnek a <strong>PORTAS</strong> <strong>bont&aacute;s n&eacute;k&uuml;li ajt&oacute;fel&uacute;j&iacute;t&aacute;s</strong> speci&aacute;lis technol&oacute;gi&aacute;j&aacute;val.</p>\r\n				<p>\r\n					<strong>Helysz&iacute;n:</strong> 2500 Esztergom, Sz&eacute;ch&eacute;nyi t&eacute;r 16. (volt Belga &eacute;tterem)</p>\r\n				<p>\r\n					<span style="line-height:20.7999992370605px">A bemutat&oacute; k&ouml;telezetts&eacute;g n&eacute;lk&uuml;li, melyről senki nem megy haza &uuml;res k&eacute;zzel!!</span></p>\r\n				<p>\r\n					Nyitott kapuval v&aacute;rjuk &Ouml;n&ouml;ket!</p>\r\n				<h2 style="font-style: italic;">\r\n					...a PORTAS ajt&oacute;fel&uacute;j&iacute;t&aacute;s &quot;Sima-&uuml;gy&quot;</h2>\r\n				<div class="image_wrapper">\r\n					&nbsp;</div>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<table border="0" height="111" width="750">\r\n	<tbody>\r\n		<tr>\r\n			<td style="padding-right:20px" valign="top">\r\n				<h4>\r\n					<strong style="color: #7d0d0b;">CONSTRUMA 2015 - &quot;A&quot; 105a STAND,</strong><strong style="color: #7d0d0b;"> 2015-04-18</strong></h4>\r\n				<p>\r\n					Szeretettel v&aacute;rjuk r&eacute;gi &eacute;s &uacute;j &uuml;gyfeleinket, partnereinket!</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<table border="0">\r\n	<tbody>\r\n		<tr>\r\n			<td style="padding-right:20px" valign="top">\r\n				<h4>\r\n					<strong style="color: #7d0d0b;">CONSTRUMA 2015,</strong><strong style="color: #7d0d0b;"> 2015-03-25</strong></h4>\r\n				<p>\r\n					CONSTRUMA &Eacute;P&Iacute;TŐIPARI SZAKKI&Aacute;LL&Iacute;T&Aacute;S</p>\r\n				<p>\r\n					2015. &aacute;prilis 15-19.</p>\r\n				<p>\r\n					L&Aacute;TOGASSON MEG MINKET A CONSTRUM&Aacute;N!</p>\r\n				<p>\r\n					Keressen minket az A pavilon 105A standj&aacute;n, ahol megtekintheti mintaajtajainkat &eacute;s koll&eacute;g&aacute;ink r&eacute;szletes t&aacute;j&eacute;koztat&aacute;st ny&uacute;jtanak a PORTAS - mint egyed&uuml;l&aacute;ll&oacute;, bont&aacute;s n&eacute;lk&uuml;li - AJT&Oacute;FEL&Uacute;J&Iacute;T&Aacute;S lehetős&eacute;geiről &eacute;s előnyeiről.</p>\r\n				<p>\r\n					Szeretettel v&aacute;juk a Construma ideje alatti jelentős kedvezm&eacute;nyeinkkel!</p>\r\n			</td>\r\n			<td>\r\n				<div class="image_wrapper">\r\n					&nbsp;</div>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<table border="0">\r\n	<tbody>\r\n		<tr>\r\n			<td style="padding-right:20px" valign="top">\r\n				<h4>\r\n					<strong style="color: #7d0d0b;">KAR&Aacute;CSONYI KEDVEZM&Eacute;NYEK,</strong><strong style="color: #7d0d0b;"> 2014-11-01</strong></h4>\r\n				<p>\r\n					<strong>LAST MINUTE AJ&Aacute;NLAT!</strong></p>\r\n				<p>\r\n					A <em><strong>2014 december 24</strong></em>-<strong><em>ig</em></strong> megk&ouml;t&ouml;tt szerződ&eacute;sekre<strong> jelentős kedvezm&eacute;nyeket</strong> adunk - ig&eacute;ny szerint v&aacute;laszthat&oacute;an - egy&eacute;ni megbesz&eacute;l&eacute;s alapj&aacute;n!</p>\r\n				<p>\r\n					H&iacute;vjon minket bizalommal!</p>\r\n				<p>\r\n					+36 70/419-9000</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<p>\r\n	&nbsp;</p>\r\n', 0, '', 0, 1, '', '', '', '', '', ''),
(57, 'Ajtó felújítása', 'ajto_felujitasa', '<table border="0">\r\n	<tbody>\r\n		<tr>\r\n			<td style="padding-right:40px">\r\n				<h4>\r\n					PROBL&Eacute;MA - FEST&Eacute;S, M&Aacute;ZOL&Aacute;S</h4>\r\n				<p>\r\n					A fest&eacute;s mindig ideiglenes megold&aacute;s marad. A fest&eacute;k &eacute;vről-&eacute;vre &uacute;jra &eacute;s &uacute;jra lekopik, a haszn&aacute;latt&oacute;l lepereg, az ajt&oacute; illeszt&eacute;si reped&eacute;sei pedig mindig &uacute;jra előb&uacute;jnak.</p>\r\n				<p>\r\n					<strong style="color: #7d0d0b;">MEGOLD&Aacute;S?</strong></p>\r\n			</td>\r\n			<td>\r\n				<div class="image_wrapper">\r\n					<img src="http://www.ajtofelujitas-dunakanyar.hu/images/festmaz.jpg" /></div>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<table border="0">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n				<div class="image_wrapper">\r\n					<img src="http://www.ajtofelujitas-dunakanyar.hu/images/epitesi_ter.jpg" /></div>\r\n			</td>\r\n			<td style="padding-left:30px">\r\n				<h4>\r\n					PROBL&Eacute;MA - AJT&Oacute; CSERE</h4>\r\n				<p>\r\n					A lak&aacute;s &eacute;p&iacute;t&eacute;si ter&uuml;lett&eacute; v&aacute;ltozik, ahol az egykor stabilan be&eacute;p&iacute;tett belt&eacute;ri ajt&oacute;k kibont&aacute;sa is sokszor komoly rombol&aacute;ssal j&aacute;r, a t&eacute;gl&aacute;k meglazulnak, n&eacute;hol a ny&iacute;l&aacute;s&aacute;thidal&oacute; is hi&aacute;nyzik.Ilyenkor a kőművesre, a burkol&oacute;ra, asztalosra &eacute;s a festőre is sz&uuml;ks&eacute;g van a helyre&aacute;ll&iacute;t&aacute;shoz. Nem besz&eacute;lve az ut&oacute;lagos meglepet&eacute;sekről a k&ouml;lts&eacute;gek ter&eacute;n.</p>\r\n				<p>\r\n					<strong style="color: #7d0d0b;">MEGOLD&Aacute;S?</strong></p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<h4 style="color: #7d0d0b;">\r\n	PORTAS MEGOLD&Aacute;S - Bont&aacute;s n&eacute;lk&uuml;li fel&uacute;j&iacute;t&aacute;s</h4>\r\n<ul class="pipa_list">\r\n	<li>\r\n		Megl&eacute;vőből &bdquo;&uacute;j&rdquo; lesz ak&aacute;r 1 nap alatt</li>\r\n	<li>\r\n		Kiv&aacute;l&oacute; minős&eacute;gű &ndash; N&eacute;metorsz&aacute;gban gy&aacute;rtott &ndash; PORTAS alapanyagok</li>\r\n	<li>\r\n		V&aacute;laszt&aacute;si lehetős&eacute;g t&ouml;bb mint 1000 f&eacute;le modell k&ouml;z&uuml;l</li>\r\n	<li>\r\n		Szakszerű tan&aacute;csad&aacute;s &eacute;s előre tervezett kivitelez&eacute;si hat&aacute;ridő</li>\r\n	<li>\r\n		Egyedi ig&eacute;nyek &eacute;s k&iacute;v&aacute;ns&aacute;gok korl&aacute;tlan megval&oacute;s&iacute;that&oacute;s&aacute;ga</li>\r\n	<li>\r\n		Fel&uacute;j&iacute;t&aacute;s ut&aacute;n nincs sz&uuml;ks&eacute;g t&ouml;bb&eacute; fest&eacute;sre, m&aacute;zol&aacute;sra, megszűnnek a reped&eacute;sek &eacute;s kop&aacute;sok</li>\r\n	<li>\r\n		Nagyon k&ouml;nnyen &aacute;polhat&oacute; fel&uuml;let</li>\r\n</ul>\r\n<div class="image_wrapper">\r\n	<img src="http://www.ajtofelujitas-dunakanyar.hu/images/folyamat.jpg" /></div>\r\n', 0, '', 0, 1, '', '', '', '', '', ''),
(58, 'Tokfelújítás', 'tokfelujitas', '<table border="0">\r\n	<tbody>\r\n		<tr>\r\n			<td style="padding-right:40px">\r\n				<p>\r\n					<span style="font-size:14px;">A PORTAS &aacute;ltal fel&uacute;j&iacute;tott ajt&oacute; a hozz&aacute; illő ajt&oacute;kerettel lesz igaz&aacute;n t&ouml;k&eacute;letes.</span></p>\r\n				<span style="font-size:14px;">PORTAS m&oacute;dszerrel a tokfel&uacute;j&iacute;t&aacute;s is minden gond n&eacute;lk&uuml;l zajlik.&nbsp; Egyszerűen &eacute;s t&ouml;k&eacute;letesen megval&oacute;s&iacute;that&oacute; b&aacute;rmilyen ig&eacute;ny, legyen az ak&aacute;r az ajt&oacute;-dekort&oacute;l elt&eacute;rő, vagy sz&iacute;nazonos mint&aacute;zat, vagy ak&aacute;r az &Ouml;n &iacute;zl&eacute;se szerinti, berendez&eacute;shez jobban harmoniz&aacute;l&oacute; d&iacute;szkeret.</span>\r\n				<h4>\r\n					&nbsp;</h4>\r\n			</td>\r\n			<td>\r\n				<div class="image_wrapper">\r\n					<img src="http://www.ajtofelujitas-dunakanyar.hu/images/tokfel.jpg" /></div>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<p>\r\n	&nbsp;</p>\r\n', 0, '', 0, 1, '', '', '', '', '', ''),
(59, 'Ötletadó', 'otletado', '<p>\r\n	A PORTAS legjobb referenciamunk&aacute;i, melyek kezdő &ouml;tletk&eacute;nt szolg&aacute;hatnak b&aacute;rkinek, el&eacute;gedett &uuml;gyfeleinkn&eacute;l l&aacute;that&oacute;k.<br />\r\n	&nbsp;</p>\r\n<table align="center" border="0" height="522" style="margin: auto" width="513">\r\n	<tbody>\r\n		<tr>\r\n			<td style="padding:3px">\r\n				<div class="box_w270 float_l">\r\n					<div class="image_wrapper">\r\n						<img alt="image" src="http://www.ajtofelujitas-dunakanyar.hu/images/eu1.jpg" /></div>\r\n				</div>\r\n			</td>\r\n			<td style="padding:3px">\r\n				<img alt="image" src="http://www.ajtofelujitas-dunakanyar.hu/images/eu4.jpg" /></td>\r\n		</tr>\r\n		<tr>\r\n			<td style="padding:3px">\r\n				<div class="box_w270 float_r">\r\n					<div class="image_wrapper">\r\n						<img alt="image" src="http://www.ajtofelujitas-dunakanyar.hu/images/eu2.jpg" /></div>\r\n				</div>\r\n			</td>\r\n			<td style="padding:3px">\r\n				<div class="box_w270 float_l">\r\n					<div class="image_wrapper">\r\n						<img alt="image" src="http://www.ajtofelujitas-dunakanyar.hu/images/eu3.jpg" /></div>\r\n				</div>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<div class="box_w270 float_l">\r\n	<div class="image_wrapper">\r\n		&nbsp;</div>\r\n</div>\r\n<div class="box_w270 float_r">\r\n	<div class="image_wrapper">\r\n		&nbsp;</div>\r\n</div>\r\n<div class="box_w270 float_l">\r\n	<div class="image_wrapper">\r\n		&nbsp;</div>\r\n</div>\r\n<div class="box_w270 float_r">\r\n	<div class="image_wrapper">\r\n		&nbsp;</div>\r\n</div>\r\n<p>\r\n	&nbsp;</p>\r\n', 0, '', 0, 1, '', '', '', '', '', ''),
(60, 'Garancia', 'garancia', '<div class="row">\r\n<div class="col-md-8">\r\n				<p>\r\n					A PORTAS speci&aacute;lis burkol&oacute;anyagot kiv&aacute;l&oacute; minős&eacute;gű nyers &eacute;s seg&eacute;danyagokb&oacute;l &aacute;ll&iacute;tj&aacute;k elő.</p>\r\n				<p>\r\n					Elő&aacute;ll&iacute;t&aacute;sa minős&eacute;g-ellenőrz&eacute;s mellett t&ouml;rt&eacute;nik, &iacute;gy a felhaszn&aacute;l&oacute;nak nagy gy&aacute;rt&aacute;si biztons&aacute;got ny&uacute;jt, a k&eacute;szterm&eacute;k pedig sokoldal&uacute; haszn&aacute;lhat&oacute;s&aacute;got.</p>\r\n				<p>\r\n					A PORTAS burkol&oacute;anyagot Eur&oacute;p&aacute;ban csak a kiz&aacute;r&oacute;lagos forgalmaz&oacute;i joggal rendelkező PORTAS-Szak&uuml;zemek haszn&aacute;latj&aacute;k k&uuml;l&ouml;nb&ouml;ző ajt&oacute;k fel&uacute;j&iacute;t&aacute;s&aacute;ra.</p>\r\n				<p>\r\n					A PORTAS bevon&oacute;anyagokat a Rosenheimi B&uacute;tortechnikai Minős&iacute;tő Int&eacute;zet vizsg&aacute;lta &eacute;s garant&aacute;lja a legjobb minős&eacute;get.</p>\r\n				<p>\r\n					A PORTAS anyag &uuml;t&eacute;s-, l&ouml;k&eacute;s-, szak&iacute;t&oacute;- &eacute;s kop&aacute;s&aacute;ll&oacute;.</p>\r\n				<p>\r\n					A PORTAS anyac&eacute;g 12 &eacute;v sz&iacute;ngaranci&aacute;t v&aacute;llal a v&aacute;lasztott sz&iacute;n p&oacute;tl&aacute;s&aacute;ra &eacute;s sz&uuml;ks&eacute;ges t&ouml;bbletanyagra.</p>\r\n				<p>\r\n					A PORTAS 40 &eacute;ve fenn&aacute;ll&oacute; tapasztalati garanci&aacute;ja biztos&iacute;tja a Vevők ig&eacute;nyeinek legmagasabb fok&uacute; kiszolg&aacute;l&aacute;s&aacute;t.</p>\r\n				<p>\r\n					C&eacute;g&uuml;nk v&aacute;llalja a Magyar Jogszab&aacute;ly &aacute;ltal elő&iacute;rt 2 &eacute;v &ndash; fel&uacute;j&iacute;t&aacute;sra vonatkoz&oacute; &ndash; garanci&aacute;t.</p>\r\n			</div>\r\n			<div class="col-md-4">\r\n				<div class="image_wrapper">\r\n					<img src="http://www.ajtofelujitas-dunakanyar.hu/images/gari.jpg" /></div>\r\n			</div>\r\n</div>\r\n<div class="row">\r\n			<div class="col-md-4">\r\n				<div class="image_wrapper">\r\n					<img src="http://www.ajtofelujitas-dunakanyar.hu/images/minbiz.jpg" /></div>\r\n			</div>\r\n			<div class="col-md-8">\r\n				<h4>\r\n					A DIN 53389 szabv&aacute;ny szerint</h4>\r\n				<ul>\r\n					<li>\r\n						F&eacute;ny&aacute;ll&oacute;s&aacute;ga nagyon j&oacute;;</li>\r\n					<li>\r\n						Hő ellen&aacute;ll&oacute; k&eacute;pess&eacute;ge +72 &deg;C-ig;</li>\r\n					<li>\r\n						Fagy&aacute;ll&oacute; k&eacute;pess&eacute;ge -22 &deg;C-ig;</li>\r\n					<li>\r\n						Tűzelh&aacute;r&iacute;t&aacute;si k&eacute;pess&eacute;ge: &ouml;n&aacute;ll&oacute;an nem gyullad meg, tűz eset&eacute;n mag&aacute;t&oacute;l elolt&oacute;dik.</li>\r\n				</ul>\r\n			</div>\r\n		</div>\r\n<p>\r\n	<br />\r\n	A PORTAS burkol&oacute;anyag b&aacute;rmilyen forgalomban l&eacute;vő k&ouml;nnyű tiszt&iacute;t&oacute;szeres, langyos v&iacute;zzel tiszt&iacute;that&oacute;.<br />\r\n	A nem rendeltet&eacute;sszerű haszn&aacute;latb&oacute;l eredő szennyeződ&eacute;sek (c&iacute;pőtalp lenyomat, &uacute;jj lenyomat, ceruza &eacute;s egy&eacute;b foltok) elt&aacute;vol&iacute;t&aacute;s&aacute;hoz a PORTAS tiszt&iacute;t&oacute;szett haszn&aacute;lat&aacute;t aj&aacute;nljuk, melyet az &Ouml;nh&ouml;z legk&ouml;zelebbi PORTAS-Szak&uuml;zemben v&aacute;s&aacute;rolhat meg.</p>\r\n<p>\r\n	FIGYELEM!<br />\r\n	K&eacute;rj&uuml;k, ne haszn&aacute;ljon b&uacute;torf&eacute;nyt, viaszt, vagy hasonl&oacute; &aacute;pol&oacute;szert!</p>\r\n', 0, '', 0, 1, '', '', '', '', '', ''),
(62, 'Bejárati ajtók felújítása', 'bejarati_ajtok_felujitasa', '<div class="row">\r\n	<div class="col-md-4">\r\n		<div class="image_wrapper">\r\n			<img src="http://www.ajtofelujitas-dunakanyar.hu/images/kulso1.jpg" /></div>\r\n	</div>\r\n	<div class="col-md-8">\r\n		<p>\r\n			<strong style="color: #7d0d0b;">MUTAT&Oacute;S &Eacute;S BIZTONS&Aacute;GOS</strong></p>\r\n		<p>\r\n			A bej&aacute;rati ajt&oacute; fel&uacute;j&iacute;t&aacute;sakor sok krit&eacute;riumnak kell megfeleln&uuml;nk. Az időj&aacute;r&aacute;snak, napsug&aacute;rz&aacute;snak kitett fel&uuml;letnek t&ouml;k&eacute;letes ellen&aacute;ll&oacute; k&eacute;pess&eacute;ggel kell b&iacute;rnia, mindezt minős&eacute;gi kivitelben, hiszen a bej&aacute;rati ajt&oacute; az &eacute;p&uuml;let egyik legkiemelkedőbb d&iacute;sze.</p>\r\n		<p>\r\n			A m&aacute;sik legfontosabb elv&aacute;r&aacute;s a bej&aacute;rati ajt&oacute;val szemben a biztons&aacute;g.</p>\r\n		<p>\r\n			<strong style="color: #7d0d0b;">A bont&aacute;s n&eacute;lk&uuml;li PORTAS-megold&aacute;s mindezeknek t&ouml;k&eacute;letesen megfelel!</strong></p>\r\n		<p>\r\n			&Aacute;ltal&aacute;ban egy nap alatt elk&eacute;sz&iacute;thető a bej&aacute;rati ajt&oacute; fel&uacute;j&iacute;t&aacute;sa. A fel&uacute;j&iacute;t&aacute;s ideje alatt a bej&aacute;rati ajt&oacute;sz&aacute;rny hely&eacute;re egy speci&aacute;lisan kialak&iacute;tott ideiglenes ajt&oacute;val l&aacute;tjuk el.</p>\r\n	</div>\r\n</div>\r\n', 0, '', 0, 1, '', '', '', '', '', ''),
(63, 'Minőség és biztonság', 'minoseg_es_biztonsag', '<div class="row">\r\n			<div class="col-md-8">\r\n				<p>\r\n					<strong style="color: #7d0d0b;">Minős&eacute;g</strong></p>\r\n				<p>\r\n					A PORTAS speci&aacute;lis bej&aacute;rati ajt&oacute; burkol&oacute;anyagot kiv&aacute;l&oacute; minős&eacute;gű nyers &eacute;s seg&eacute;danyagokb&oacute;l &aacute;ll&iacute;tj&aacute;k elő. <strong>Elő&aacute;ll&iacute;t&aacute;sa minős&eacute;g-ellenőrz&eacute;s mellett t&ouml;rt&eacute;nik</strong>, &iacute;gy a felhaszn&aacute;l&oacute;nak nagy gy&aacute;rt&aacute;si biztons&aacute;got ny&uacute;jt, a k&eacute;szterm&eacute;k pedig sokoldal&uacute; haszn&aacute;lhat&oacute;s&aacute;got.</p>\r\n				<p>\r\n					A PORTAS burkol&oacute;anyagot Eur&oacute;p&aacute;ban csak a kiz&aacute;r&oacute;lagos forgalmaz&oacute;i joggal rendelkező PORTAS-Szak&uuml;zemek haszn&aacute;latj&aacute;k k&uuml;l&ouml;nb&ouml;ző ajt&oacute;k fel&uacute;j&iacute;t&aacute;s&aacute;ra. A PORTAS bevon&oacute;anyagokat a <strong>Rosenheimi B&uacute;tortechnikai Minős&iacute;tő Int&eacute;zet</strong> vizsg&aacute;lta &eacute;s garant&aacute;lja a legjobb minős&eacute;get.</p>\r\n				<p>\r\n					A PORTAS bej&aacute;rati ajt&oacute; burkol&oacute;anyag <strong>időj&aacute;r&aacute;s- &eacute;s t&ouml;r&eacute;s&aacute;ll&oacute;</strong>. A PORTAS anyac&eacute;g <strong>12 &eacute;v sz&iacute;ngaranci&aacute;t</strong> v&aacute;llal a v&aacute;lasztott sz&iacute;n p&oacute;tl&aacute;s&aacute;ra &eacute;s sz&uuml;ks&eacute;ges t&ouml;bbletanyagra. A PORTAS 40 &eacute;ve fenn&aacute;ll&oacute; tapasztalati garanci&aacute;ja biztos&iacute;tja a Vevők ig&eacute;nyeinek legmagasabb fok&uacute; kiszolg&aacute;l&aacute;s&aacute;t.</p>\r\n			</div>\r\n			<div class="col-md-4">\r\n				<div class="image_wrapper">\r\n					<a href="http://www.ajtofelujitas-dunakanyar.hu/images/min_biz_n.jpg"><img height="339" src="http://www.ajtofelujitas-dunakanyar.hu/images/min_biz_k.jpg" width="179" /></a></div>\r\n			</div>\r\n		</div>\r\n<table border="0">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n				<div class="image_wrapper">\r\n					<a href="http://www.ajtofelujitas-dunakanyar.hu/images/bizt_nagy.jpg"><img src="http://www.ajtofelujitas-dunakanyar.hu/images/bizt_kicsi.jpg" /></a></div>\r\n			</td>\r\n			<td style="padding-left:20px" valign="top">\r\n				<p>\r\n					<strong style="color: #7d0d0b;">Biztons&aacute;g</strong></p>\r\n				<p>\r\n					A PORTAS k&uuml;l&ouml;n&ouml;sen a bej&aacute;rati ajt&oacute;k fel&uacute;j&iacute;t&aacute;sa eset&eacute;n k&uuml;l&ouml;n&ouml;s gondot ford&iacute;t a<strong> lehető legbiztons&aacute;gosabb</strong> ajt&oacute; kivitelez&eacute;s&eacute;re.</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n', 0, '', 0, 1, '', '', '', '', '', ''),
(64, 'Előtetők', 'elotetok', '<div class="row">\r\n	<div class="col-md-8">\r\n		<p>\r\n			<strong style="color: #7d0d0b;">Előtetők anyaga / fel&uuml;letv&eacute;delem:</strong></p>\r\n		<ul>\r\n			<li>\r\n				Tart&oacute;szerkezetek: Nemesac&eacute;l szinterezett vagy nemesac&eacute;l fel&uuml;lettel.</li>\r\n			<li>\r\n				Az előtetők a szerkezeti kialak&iacute;t&aacute;suk szerint lehetnek: &Iacute;velt vagy egyenes fel&uuml;lettel.</li>\r\n			<li>\r\n				Fedőanyagok - &Uuml;vegek: Edzett - &iacute;velt vagy egyenes kivitelben.</li>\r\n		</ul>\r\n	</div>\r\n	<div class="col-md-4">\r\n		<div class="image_wrapper">\r\n			<img src="http://www.ajtofelujitas-dunakanyar.hu/images/elot1.jpg" /></div>\r\n	</div>\r\n</div>\r\n<p>\r\n	&nbsp;</p>\r\n<div class="row">\r\n	<div class="col-md-4">\r\n		<div class="image_wrapper">\r\n			<img src="http://www.ajtofelujitas-dunakanyar.hu/images/elot2.jpg" /></div>\r\n	</div>\r\n	<div class="col-md-8">\r\n		<ul>\r\n			<li>\r\n				A porsz&oacute;r&aacute;ssal kezelt fel&uuml;letek, a vevőink ig&eacute;nye szerint b&aacute;rmilyen RAL sz&iacute;nben, egyedileg k&eacute;sz&uuml;lnek.</li>\r\n			<li>\r\n				A tart&oacute;szerkezet r&ouml;gz&iacute;t&eacute;se minden modell eset&eacute;ben edzett tart&oacute;csavarokkal t&ouml;rt&eacute;nik.</li>\r\n			<li>\r\n				Sz&eacute;p, st&iacute;lusos &eacute;s időt&aacute;ll&oacute;. Ez jellemzi minden modell&uuml;nket.</li>\r\n		</ul>\r\n	</div>\r\n</div>\r\n', 0, '', 0, 1, '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `slider`
--

CREATE TABLE IF NOT EXISTS `slider` (
  `id` int(11) NOT NULL,
  `sorrend` int(11) NOT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `file` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(5) COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `statusz` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `slider`
--

INSERT INTO `slider` (`id`, `sorrend`, `nev`, `file`, `nyelv`, `url`, `statusz`) VALUES
(1, 0, 'Üdvözöljük a Partner Mester Kft Weboldalán!', '57e12-door-knob.jpg', '', '', 1);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `termekek`
--

CREATE TABLE IF NOT EXISTS `termekek` (
  `id` int(11) NOT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `kategoria` int(11) NOT NULL,
  `lead` text COLLATE utf8_hungarian_ci NOT NULL,
  `leiras` text COLLATE utf8_hungarian_ci NOT NULL,
  `fokep` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `title` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `keywords` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `description` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `header_custom_code` text COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(5) COLLATE utf8_hungarian_ci NOT NULL,
  `statusz` int(1) NOT NULL,
  `gyarto` int(11) NOT NULL,
  `marka` int(11) NOT NULL,
  `kiemelt` int(11) NOT NULL,
  `ar` int(11) NOT NULL,
  `ar_akcios` int(11) NOT NULL,
  `valuta` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `ar_beszerzesi` int(11) NOT NULL,
  `suly` int(11) NOT NULL,
  `raktarkeszlet` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `termek_kepek`
--

CREATE TABLE IF NOT EXISTS `termek_kepek` (
  `id` int(11) NOT NULL,
  `title` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `file` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `termek` int(11) NOT NULL,
  `sorrend` int(11) NOT NULL,
  `datum` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `termek_tulajdonsagok`
--

CREATE TABLE IF NOT EXISTS `termek_tulajdonsagok` (
  `id` int(11) NOT NULL,
  `termek` int(11) NOT NULL,
  `tulajdonsag_id` int(11) NOT NULL,
  `tulajdonsag` varchar(256) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `tulajdonsag`
--

CREATE TABLE IF NOT EXISTS `tulajdonsag` (
  `id` int(11) NOT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `szulo` int(200) NOT NULL,
  `nyelv` varchar(5) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `tulajdonsag_kat`
--

CREATE TABLE IF NOT EXISTS `tulajdonsag_kat` (
  `id` int(11) NOT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(256) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `name` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `password` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `perm` varchar(128) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `users`
--

INSERT INTO `users` (`id`, `name`, `password`, `perm`) VALUES
(2, 'admin', 'c93ccd78b2076528346216b3b2f701e6', '1');

--
-- Indexek a kiírt táblákhoz
--

--
-- A tábla indexei `beallitasok`
--
ALTER TABLE `beallitasok`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `companytype`
--
ALTER TABLE `companytype`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `dokumentumok`
--
ALTER TABLE `dokumentumok`
  ADD PRIMARY KEY (`dokumentumokid`);

--
-- A tábla indexei `galeria`
--
ALTER TABLE `galeria`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `gyarto`
--
ALTER TABLE `gyarto`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `gyik`
--
ALTER TABLE `gyik`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `hirek`
--
ALTER TABLE `hirek`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `hirek_kategoria`
--
ALTER TABLE `hirek_kategoria`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `kategoria`
--
ALTER TABLE `kategoria`
  ADD PRIMARY KEY (`kategoriaid`);

--
-- A tábla indexei `kategoriak`
--
ALTER TABLE `kategoriak`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `kerdezzfelelek`
--
ALTER TABLE `kerdezzfelelek`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `marka`
--
ALTER TABLE `marka`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `megjelenes`
--
ALTER TABLE `megjelenes`
  ADD PRIMARY KEY (`megjelenesid`);

--
-- A tábla indexei `nyelvi_forditasok`
--
ALTER TABLE `nyelvi_forditasok`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `oldalak`
--
ALTER TABLE `oldalak`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `termekek`
--
ALTER TABLE `termekek`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `termek_kepek`
--
ALTER TABLE `termek_kepek`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `termek_tulajdonsagok`
--
ALTER TABLE `termek_tulajdonsagok`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `tulajdonsag`
--
ALTER TABLE `tulajdonsag`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `tulajdonsag_kat`
--
ALTER TABLE `tulajdonsag_kat`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- A kiírt táblák AUTO_INCREMENT értéke
--

--
-- AUTO_INCREMENT a táblához `beallitasok`
--
ALTER TABLE `beallitasok`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT a táblához `companytype`
--
ALTER TABLE `companytype`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT a táblához `dokumentumok`
--
ALTER TABLE `dokumentumok`
  MODIFY `dokumentumokid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `galeria`
--
ALTER TABLE `galeria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT a táblához `gyarto`
--
ALTER TABLE `gyarto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `gyik`
--
ALTER TABLE `gyik`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `hirek`
--
ALTER TABLE `hirek`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `hirek_kategoria`
--
ALTER TABLE `hirek_kategoria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `kategoria`
--
ALTER TABLE `kategoria`
  MODIFY `kategoriaid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `kategoriak`
--
ALTER TABLE `kategoriak`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `kerdezzfelelek`
--
ALTER TABLE `kerdezzfelelek`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `marka`
--
ALTER TABLE `marka`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `megjelenes`
--
ALTER TABLE `megjelenes`
  MODIFY `megjelenesid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `nyelvi_forditasok`
--
ALTER TABLE `nyelvi_forditasok`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `oldalak`
--
ALTER TABLE `oldalak`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=65;
--
-- AUTO_INCREMENT a táblához `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT a táblához `termekek`
--
ALTER TABLE `termekek`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `termek_kepek`
--
ALTER TABLE `termek_kepek`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `termek_tulajdonsagok`
--
ALTER TABLE `termek_tulajdonsagok`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `tulajdonsag`
--
ALTER TABLE `tulajdonsag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `tulajdonsag_kat`
--
ALTER TABLE `tulajdonsag_kat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
